# Examples

Find here examples on how to use the SPP package.

## UDP Transport

The UDP transport example sends space packets from entity A (ground) to B (satellite)
and vice versa using UDP frames. For this the routing is set up to go over a waypoint.

So the routing is (APID is given in brackets):

Ground (111) <-> Waypoint (222) <-> Satellite (333)

To run the example, in separate terminals do:

```
python local.py
python waypoint.py
python remote.py
```
