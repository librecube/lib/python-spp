from spacepacket.transport import UdpTransport


# defines the routing such that data is routed between the two endpoints
udp_transport = UdpTransport(
    routing={
        ("127.0.0.1", 5111): [("127.0.0.1", 5333)],
        ("127.0.0.1", 5333): [("127.0.0.1", 5111)],
    }
)

# the adress on why the waypoint listens
udp_transport.bind("127.0.0.1", 5222)


# simply display the received (and forwarded) data
def display_data(data):
    print(data)


udp_transport.indication = display_data

try:
    input("Running. Press <Enter> to stop...\n")
except KeyboardInterrupt:
    pass

udp_transport.unbind()
