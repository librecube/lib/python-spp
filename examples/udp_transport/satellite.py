import time

from spacepacket import (
    SpacePacket,
    SpacePacketProtocolEntity,
    PacketType,
    SequenceFlags,
)
from spacepacket.transport import UdpTransport


udp_transport = UdpTransport(
    routing={
        "*": [("127.0.0.1", 5222)],
    }
)

udp_transport.bind("127.0.0.1", 5333)

entity = SpacePacketProtocolEntity(apid=333, transport=udp_transport)


def display_received_packet(packet):
    print("Received Space Packet:")
    attrs = vars(packet)
    print("  " + "\n  ".join("%s: %s" % item for item in attrs.items()))
    print()


entity.indication = display_received_packet

try:
    input("Press <Enter> to start sending packets. Then press <Ctr-C> to stop.\n")
    i = 1

    while True:

        packet = SpacePacket(
            packet_type=PacketType.TELEMETRY,
            packet_sec_hdr_flag=False,
            apid=333,
            sequence_flags=SequenceFlags.UNSEGMENTED,
            packet_sequence_count=i,
            packet_data_field=b"This is dummy telemetry",
        )
        entity.request(packet)
        i += 1
        time.sleep(2)

except KeyboardInterrupt:
    pass

udp_transport.unbind()
